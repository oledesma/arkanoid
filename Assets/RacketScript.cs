﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RacketScript : MonoBehaviour {
	public float speed = 150.0f;

	// Se utiliza FixedUpdate cuando se trabaja en las físicas.
	void FixedUpdate () {
		float horizontalDirection = Input.GetAxisRaw ("Horizontal");

		GetComponent<Rigidbody2D> ().velocity = Vector2.right * horizontalDirection * speed;
	}
}
